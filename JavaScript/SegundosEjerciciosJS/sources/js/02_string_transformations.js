const cambiarTexto = () => {
  let original_text = document.getElementById("original_text").value;
  let cadenaMostrarCapitalizada = "";
  let camelCase = "";
  let mimimi = "";
  let reshulonCad = "";

  let TransformationSelected = document.querySelector(
    'input[name="transformation"]:checked'
  ).value;

  switch (TransformationSelected) {
    case "upper_case":
      document.getElementById("transformed_text").value =
        original_text.toUpperCase();
      break;

    case "lower_case":
      document.getElementById("transformed_text").value =
        original_text.toLowerCase();
      break;

    case "capitalize":
      //Hacer con el método split
      cadenaMostrarCapitalizada = primeraLetraMayus(original_text);
      document.getElementById("transformed_text").value =
        cadenaMostrarCapitalizada;
      break;

    case "camel_case":
      camelCase = camelCaseOption(original_text);
      document.getElementById("transformed_text").value = camelCase;
      break;

    case "mimimi":
      mimimi = mimimiOption(original_text);
      document.getElementById("transformed_text").value = mimimi;
      break;

    case "reshulon":
      reshulonCad = reshulonOption(original_text);
      document.getElementById("transformed_text").value =
        reshulonCad;
      break;
  }
};

const primeraLetraMayus = (cadena) => {
  let cadenaNueva = cadena[0].toUpperCase();
  let mayuscula = false;

  for (let index = 1; index < cadena.length; index++) {
    if (cadena[index] == " " || cadena[index].toUpperCase == " ") {
      mayuscula = true;
    }

    if (mayuscula) {
      cadenaNueva = cadenaNueva + " " + cadena[index + 1].toUpperCase();
      index++;
      mayuscula = false;
    } else {
      cadenaNueva = cadenaNueva + cadena[index];
    }
  }
  return cadenaNueva;
};

const camelCaseOption = (cadena) => {
  let arrayCadena = cadena.split(" ");
  let cadenaNueva = "";

  for (let index = 0; index < arrayCadena.length; index++) {
    cadenaNueva +=
      arrayCadena[index][0].toUpperCase() + arrayCadena[index].substr(1);
  }

  return cadenaNueva;
};

const mimimiOption = (cadena) => {
  const ARRAY_VOCALES = ["A", "E", "O", "U"];
  let vocal = false;
  let cadenaNueva = "";

  for (let index = 0; index < cadena.length; index++) {
    if (ARRAY_VOCALES.indexOf(cadena[index].toUpperCase()) != -1) {
      vocal = true;
    }

    if (vocal == true) {
      cadenaNueva += "i";
      vocal = false;
    } else {
      cadenaNueva += cadena[index];
    }
  }
  return cadenaNueva;
};

const reshulonOption = (cadena) => {
  let cadenaNueva = "";

  for (let index = 0; index < cadena.length; index++) {
    let rand = Math.floor(Math.random() *2);
    
    if (rand == 1) {
      cadenaNueva += cadena[index].toUpperCase();
    }else{
      cadenaNueva += cadena[index];
    }
  }

  return cadenaNueva;
};