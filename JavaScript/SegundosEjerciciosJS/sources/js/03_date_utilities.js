const horita = () => {
  let date = new Date();
  let dia;
  let mes;

  const dias = [
    "domingo",
    "lunes",
    "martes",
    "miércoles",
    "jueves",
    "viernes",
    "sábado",
  ];
  const meses = [
    "enero",
    "febrero",
    "marzo",
    "abril",
    "mayo",
    "junio",
    "julio",
    "agosto",
    "septiembre",
    "octubre",
    "noviembre",
    "diciembre",
  ];
  let opcion = document.getElementById("dates").value;
  let outPutText = "";

  switch (opcion) {
    case "date_now":
      outPutText = `Hoy es ${dias[date.getDay()]}, ${date.getDate()} de ${
        meses[date.getMonth()]
      } de ${date.getFullYear()} y son las 
      ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
      break;

    case "date_five_mins_ago":
      let hora_5_mins =
        date.getHours() +
        ":" +
        (date.getMinutes() - 5) +
        ":" +
        date.getSeconds();
      dia = dias[date.getDay()];
      mes = meses[date.getMonth()];
      outPutText = "Hace 5 minutos era " + dia + ", "+ date.getDate() +" de "+ mes +" de "+ date.getFullYear()+ " y eran las "+ hora_5_mins;
      break;

    case "date_next_week":
      let hora =
        date.getHours() +
        ":" +
        date.getMinutes() +
        ":" +
        date.getSeconds();
      dia = dias[date.getDay()];
      mes = meses[date.getMonth()];
      outPutText = "Dentro de una semana será " + dia + ", "+ (date.getDate()+7) +" de "+ mes +" de "+ date.getFullYear()+ " y eran las "+ hora;
      break;
  }

  document
    .getElementById("output_p")
    .appendChild(document.createTextNode(outPutText));
};
// 🎲🎲🎲🎲🎲🎲🎲🎲🎲