# DI-srodgen0910

Este repositorio contiene un listado con las prácticas correspondientes al módulo profesional de Diseño de Interfaces del segundo curso de Desarrollo de Aplicaciones Multiplataforma (DAM).

1. Práctica de la unidad 2, [CV en formato MarkDown](https://gitlab.com/srodgen0910/di-srodgen0910/-/blob/main/CV/cv_sergio_rodriguez_es.md)
2. Práctica de la unidad 3, [Diseño de una pantalla de login con javaSwing](https://gitlab.com/srodgen0910/di-srodgen0910/-/tree/main/JavaSwing/target/classes/org/iesvelazquez/di)