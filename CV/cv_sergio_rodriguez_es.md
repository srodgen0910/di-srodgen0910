# CURRICULUM VITAE - Sergio Rodríguez

> La cosas difíciles son aquellas que aún no has intentado.

![El Nano|200](docs/elNano.jpg)

Estudiante de formación profesional de grado superior de **desarrollo de aplicaciones multiplataforma**.

Si deseas contactar conmigo puedes mandarme un mensaje por
[correo](mailto:srodgen0910@g.educaand.es).

---

## Experiencia profesional

- **Sketchers**(12/2020-01/2021): Encargado
  - **Carrefour Camas**
    - Dependiente de la zona de calzado infantil.
    - Encargado de cambios y devoluciones.
- **Profesor particular**(03/2015-Actualmente)
  - **Profesor de matemáticas académicas**
  - **Profesor de inglés**
  - **Profesor de física y química**

## Títulos académicos

1. Título de **ESO** obtenido en el [IES Maria Inmaculada](https://iesmai.es/) de Sevilla en 2018.
2. Título de **Bachillerato** en ciencias y tecnologia obtenido en el [IES Triana](https://blogsaverroes.juntadeandalucia.es/iestriana/) de Sevilla en 2020.

## Habilidades

| Habilidad   |     Descripción      |  Experiencia |
|:----------|:-------------|-----------:|
| Java | Java es un lenguaje de programación y una plataforma informática que fue comercializada por primera vez en 1995 por Sun Microsystems.                      | :star: :star: :star: :star: |
| SQL  | SQL es un lenguaje de dominio específico, diseñado para administrar, y recuperar información de sistemas de gestión de bases de datos relacionales.       |  :star: :star: :star: :star: |
| Python  | Python es un lenguaje de programación ampliamente utilizado en las aplicaciones web, el desarrollo de software, la ciencia de datos y el machine learning (ML) | :star: |

## Tarjeta de visita

Puedes utilizar este JSON para guardar mis datos :blush:

```json
{
    "name" : "Sergio",
    "surname" : "Rodríguez Geniz",
    "phone" : [
        {
            "label" : "fijo",
            "prefix" : "+34",
            "number" : 954514879
        },
        {
            "label" : "personal",
            "prefix" : "+34",
            "number" : 608177446
        }
    ],
    "email" : "srodgen0910@g.educaand.es"
}
```
